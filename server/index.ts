import { PluginInitializerContext } from '../../../src/core/server';
import { SgKibanaDemoPluginPlugin } from './plugin';

//  This exports static code and TypeScript types,
//  as well as, Kibana Platform `plugin()` initializer.

export function plugin(initializerContext: PluginInitializerContext) {
  return new SgKibanaDemoPluginPlugin(initializerContext);
}

export { SgKibanaDemoPluginPluginSetup, SgKibanaDemoPluginPluginStart } from './types';
