/* eslint-disable @kbn/eslint/require-license-header */
import { schema } from '@kbn/config-schema';
import { INDEX_NAME, API_PATH } from '../../common';

export function defineRoutes({ router, logger, clusterClient }) {
  router.get(
    {
      path: API_PATH.ACCOUNTS,
      validate: false,
    },
    getAccounts({ clusterClient, logger })
  );

  router.delete(
    {
      path: `${API_PATH.ACCOUNTS}/{id}`,
      validate: {
        params: schema.object({
          id: schema.string(),
        }),
      },
    },
    deleteAccount({ clusterClient, logger })
  );

  router.post(
    {
      path: API_PATH.ACCOUNTS,
      validate: {
        body: schema.object({}, { unknowns: 'allow' }),
      },
    },
    addAccount({ clusterClient, logger })
  );
}

function addAccount({ clusterClient, logger }) {
  return async function (context, request, response) {
    try {
      const { body } = request;

      const resp = await clusterClient
        .asScoped(request)
        .asCurrentUser.index({ body, index: INDEX_NAME, refresh: true });

      return response.ok({ body: resp });
    } catch (error) {
      logger.error(`Delete account. ${error.stack}`);
      return response.internalError({ body: error });
    }
  };
}

function deleteAccount({ clusterClient, logger }) {
  return async function (context, request, response) {
    try {
      const {
        params: { id },
      } = request;

      const resp = await clusterClient
        .asScoped(request)
        .asCurrentUser.delete({ id, index: INDEX_NAME, refresh: true });

      return response.ok({ body: resp });
    } catch (error) {
      logger.error(`Delete account. ${error.stack}`);
      return response.internalError({ body: error });
    }
  };
}

function getAccounts({ clusterClient, logger }) {
  return async function (context, request, response) {
    try {
      const params = {
        index: INDEX_NAME,
        scroll: '30s',
        body: { query: { match_all: {} } },
      };

      const resp = [];
      // Fetch all data using the Scroll API https://www.elastic.co/guide/en/elasticsearch/reference/current/scroll-api.html
      for await (const hit of scrollSearch(params, clusterClient.asScoped(request).asCurrentUser)) {
        resp.push({ _id: hit._id, ...hit._source });
      }

      return response.ok({ body: resp });
    } catch (error) {
      logger.error(`Get accounts. ${error.stack}`);
      return response.internalError({ body: error });
    }
  };
}

async function* scrollSearch(params, client) {
  let resp = await client.search(params);

  while (true) {
    const hits = resp.body.hits.hits;

    if (!hits.length) break;

    for (const h of hits) {
      yield h;
    }

    if (!resp.body._scroll_id) break;

    resp = await client.scroll({
      scrollId: resp.body._scroll_id,
      scroll: params.scroll,
    });
  }
}
