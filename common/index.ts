export const PLUGIN_ID = 'sgKibanaDemoPlugin';
export const PLUGIN_NAME = 'sgKibanaDemoPlugin';

export const INDEX_NAME = 'searchguard_demo_accounts';

export const API_PATH = {
  ACCOUNTS: '/api/sg_kibana_demo_plugin/accounts',
};
