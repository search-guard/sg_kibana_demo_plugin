/* eslint-disable @kbn/eslint/require-license-header */
import React, { useState } from 'react';
import { EuiOverlayMask, EuiConfirmModal } from '@elastic/eui';

const MODALS = {
  CONFIRM_DELETION: 'confirmDeletion',
};

function ConfirmDeleteModal({
  onCancel,
  onConfirm,
  title = 'Delete',
  cancelButtonText = "No, don't do it",
  confirmButtonText = 'Yes, do it',
}) {
  return (
    <EuiOverlayMask>
      <EuiConfirmModal
        title={title}
        onCancel={onCancel}
        onConfirm={onConfirm}
        cancelButtonText={cancelButtonText}
        confirmButtonText={confirmButtonText}
        defaultFocusedButton="confirm"
        buttonColor="danger"
      >
        <p>You&rsquo;re about to do delete the resource.</p>
        <p>Are you sure you want to do this?</p>
      </EuiConfirmModal>
    </EuiOverlayMask>
  );
}

function Modal({ modal, onCancel }) {
  if (!modal) return null;

  if (modal.type === MODALS.CONFIRM_DELETION) {
    return <ConfirmDeleteModal onConfirm={modal.payload.onConfirm} onCancel={onCancel} />;
  }

  return null;
}

export const Context = React.createContext();

export function ContextProvider({ children, notifications, http }) {
  const [modal, setModal] = useState(null);

  function closeModal() {
    setModal(null);
  }

  function triggerModal(modal) {
    setModal(modal);
  }

  function triggerConfirmDeletionModal(payload) {
    const modal = payload === null ? null : { type: MODALS.CONFIRM_DELETION, payload };
    triggerModal(modal);
  }

  // Trigger success message
  function addSuccessToast(text) {
    notifications.toasts.addSuccess(text);
  }

  // Trigger error message
  function addErrorToast(error) {
    notifications.toasts.addError(error, {});
  }

  return (
    <>
      <Context.Provider
        value={{
          http,
          closeModal,
          triggerConfirmDeletionModal,
          addSuccessToast,
          addErrorToast,
        }}
      >
        {children}
      </Context.Provider>

      <Modal modal={modal} onCancel={closeModal} />
    </>
  );
}