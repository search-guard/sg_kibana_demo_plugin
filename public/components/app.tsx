/* eslint-disable @kbn/eslint/require-license-header */
/* eslint-disable max-classes-per-file */
import React, { useState, useContext, useEffect } from 'react';
import uuid from 'uuid';
import { i18n } from '@kbn/i18n';
import { FormattedMessage, I18nProvider } from '@kbn/i18n/react';
import { BrowserRouter as Router } from 'react-router-dom';

import {
  EuiHorizontalRule,
  EuiPage,
  EuiPageBody,
  EuiPageContent,
  EuiPageContentBody,
  EuiPageContentHeader,
  EuiPageHeader,
  EuiTitle,
  EuiText,
  EuiSpacer,
  EuiInMemoryTable,
  EuiButtonIcon,
} from '@elastic/eui';

import { CoreStart } from '../../../../src/core/public';
import { NavigationPublicPluginStart } from '../../../../src/plugins/navigation/public';

import { PLUGIN_ID, PLUGIN_NAME, API_PATH } from '../../common';
import { ContextProvider, Context } from './context';

class ApiService {
  constructor(props) {
    this.http = props.http;
  }

  get(path) {
    return this.http.get(path);
  }

  create(path, doc) {
    return this.http.post(path, { body: JSON.stringify(doc) });
  }

  delete(path, id) {
    return this.http.delete(`${path}/${id}`);
  }
}

class AccountService extends ApiService {
  constructor(props) {
    super(props);
  }

  get() {
    return super.get(API_PATH.ACCOUNTS);
  }

  create(doc) {
    return super.create(API_PATH.ACCOUNTS, doc);
  }

  delete(id) {
    return super.delete(API_PATH.ACCOUNTS, id);
  }
}

function TableCloneAction({ onClick }) {
  return <EuiButtonIcon onClick={onClick} color="primary" iconType="copy" aria-label="Clone" />;
}

function TableDeleteAction({ onClick }) {
  return <EuiButtonIcon onClick={onClick} color="danger" iconType="trash" aria-label="Delete" />;
}

// This custom hook saves you from a lot of code duplication if you plan
// to have multiple tables.
// https://reactjs.org/docs/hooks-custom.html
function useTableResources({ Service }) {
  const { http, addSuccessToast, addErrorToast } = useContext(Context);
  // We use the React hooks here to define the state
  // https://reactjs.org/docs/hooks-intro.html
  const [tableError, setTableError] = useState();
  const [resources, setResources] = useState([]);

  const resourceService = new Service({ http });

  // Run the code while the page is loading.
  useEffect(() => {
    fetchResources();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function fetchResources() {
    try {
      const resp = await resourceService.get();

      setResources(resp);
      setTableError(undefined);
      return resp;
    } catch (error) {
      console.error(`Fetch resources. ${error.stack}`);

      setTableError(error.message);
      addErrorToast(error);
    }
  }

  async function cloneResource(resource) {
    try {
      const { _id: id, ...doc } = resource;
      doc.name += ` Clone ${uuid.v4()}`;

      const resp = await resourceService.create(doc);
      addSuccessToast('Cloned resource.');

      fetchResources();
      return resp;
    } catch (error) {
      console.error(`Clone resource. ${error.stack}`);
      addErrorToast(error);
    }
  }

  async function deleteResource(resource) {
    try {
      const resp = await resourceService.delete(resource._id);
      addSuccessToast('Deleted resource.');

      fetchResources();
      return resp;
    } catch (error) {
      console.error(`Clone resource. ${error.stack}`);
      addErrorToast(error);
    }
  }

  return { resources, tableError, fetchResources, cloneResource, deleteResource };
}

// The root component for the table.
function AccountsTable() {
  const { triggerConfirmDeletionModal } = useContext(Context);
  const { resources, tableError, cloneResource, deleteResource } = useTableResources({
    Service: AccountService,
  });

  const actions = [
    {
      render: (resource) => <TableCloneAction onClick={() => cloneResource(resource)} />,
    },
    {
      render: (resource) => {
        return (
          <TableDeleteAction
            onClick={() => {
              triggerConfirmDeletionModal({
                onConfirm: () => {
                  deleteResource(resource);
                  triggerConfirmDeletionModal(null);
                },
              });
            }}
          />
        );
      },
    },
  ];

  function renderFriends(friends = []) {
    return (
      <div>
        <ul>
          {friends.map(({ name }) => (
            <li>{name}</li>
          ))}
        </ul>
      </div>
    );
  }

  const columns = [
    {
      field: 'name',
      name: 'Name',
    },
    {
      field: 'balance',
      name: 'Balance',
    },
    {
      field: 'gender',
      name: 'Gender',
    },
    {
      field: 'company',
      name: 'Company',
    },
    {
      field: 'phone',
      name: 'Phone',
    },
    {
      field: 'address',
      name: 'Address',
    },
    {
      field: 'registered',
      name: 'Registered',
    },
    {
      field: 'friends',
      name: 'Friends',
      render: renderFriends,
    },
    {
      align: 'right',
      actions,
    },
  ];

  const search = {
    box: {
      incremental: true,
      schema: true,
    },
  };

  // Eui table component
  // https://elastic.github.io/eui/#/tabular-content/in-memory-tables
  return (
    <>
      <EuiInMemoryTable
        items={resources}
        columns={columns}
        search={search}
        pagination={true}
        sorting={true}
        error={tableError}
      />
    </>
  );
}

interface SgKibanaDemoPluginAppDeps {
  basename: string;
  notifications: CoreStart['notifications'];
  http: CoreStart['http'];
  navigation: NavigationPublicPluginStart;
}

export const SgKibanaDemoPluginApp = ({
  basename,
  notifications,
  http,
  navigation,
}: SgKibanaDemoPluginAppDeps) => {
  // Use React hooks to manage state.
  const [timestamp, setTimestamp] = useState<string | undefined>();

  const onClickHandler = () => {
    // Use the core http service to make a response to the server API.
    http.get('/api/sg_kibana_demo_plugin/example').then((res) => {
      setTimestamp(res.time);
      // Use the core notifications service to display a success message.
      notifications.toasts.addSuccess(
        i18n.translate('sgKibanaDemoPlugin.dataUpdated', {
          defaultMessage: 'Data updated',
        })
      );
    });
  };

  // Render the application DOM.
  // Note that `navigation.ui.TopNavMenu` is a stateful component exported on the `navigation` plugin's start contract.
  return (
    <ContextProvider http={http} notifications={notifications}>
      <Router basename={basename}>
        <I18nProvider>
          <>
            <navigation.ui.TopNavMenu
              appName={PLUGIN_ID}
              showSearchBar={true}
              useDefaultBehaviors={true}
            />
            <EuiPage restrictWidth="1000px">
              <EuiPageBody>
                <EuiPageHeader>
                  <EuiTitle size="l">
                    <h1>
                      <FormattedMessage
                        id="sgKibanaDemoPlugin.helloWorldText"
                        defaultMessage="{name}"
                        values={{ name: PLUGIN_NAME }}
                      />
                    </h1>
                  </EuiTitle>
                </EuiPageHeader>
                <EuiPageContent>
                  <EuiPageContentHeader>
                    <EuiTitle>
                      <h2>
                        <FormattedMessage
                          id="sgKibanaDemoPlugin.congratulationsTitle"
                          defaultMessage="Congratulations, you have successfully created a new Kibana Plugin!"
                        />
                      </h2>
                    </EuiTitle>
                  </EuiPageContentHeader>
                  <EuiPageContentBody>
                    <AccountsTable />
                  </EuiPageContentBody>
                </EuiPageContent>
              </EuiPageBody>
            </EuiPage>
          </>
        </I18nProvider>
      </Router>
    </ContextProvider>
  );
};
