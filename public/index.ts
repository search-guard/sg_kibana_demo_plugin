import './index.scss';

import { SgKibanaDemoPluginPlugin } from './plugin';

// This exports static code and TypeScript types,
// as well as, Kibana Platform `plugin()` initializer.
export function plugin() {
  return new SgKibanaDemoPluginPlugin();
}
export { SgKibanaDemoPluginPluginSetup, SgKibanaDemoPluginPluginStart } from './types';
